import numpy as np


def add_one(x):
    return x + 1


def reverse_string(arr):
    return arr[::-1]


if __name__ == '__main__':
    print(add_one(np.array([20, 23, 50])))
    print(reverse_string("abc"))
